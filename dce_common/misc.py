"""Misc utility functions."""

import os


def is_production() -> bool:
    """Check whether DCE_ENVIRONMENT == production."""
    return os.getenv('DCE_ENVIRONMENT') == 'production'


def deployment_environment() -> str:
    """Return the deployment environment, default to staging."""
    return os.getenv('DCE_ENVIRONMENT', 'staging')
