"""Common logging functions."""
import logging
import os
import sys
import threading

CKI_LOGS_FILEDIR = '/tmp/lib.log/'
STREAM = sys.stderr
FORMAT = '%(asctime)s.%(msecs)03d - [%(levelname)s] - %(name)s - %(message)s'
FORMAT_DATE = '%Y-%m-%dT%H:%M:%S'
LOCK = threading.Lock()
CKI_HANDLER = False


def get_logger(logger_name: str) -> logging.Logger:
    """Return CKI logger or descendant."""
    # Make sure adding handler is thread safe since get_logger might not be
    # called from main
    # https://docs.python.org/3/library/logging.html#logging.basicConfig
    with LOCK:
        global CKI_HANDLER  # pylint: disable=global-statement
        cki_handler = CKI_HANDLER
        CKI_HANDLER = True

    if not cki_handler:
        # Add STREAM handler to root logger.
        root_logger = logging.getLogger()
        handler = logging.StreamHandler(STREAM)
        root_logger.addHandler(handler)
        formatter = logging.Formatter(fmt=FORMAT, datefmt=FORMAT_DATE)
        handler.setFormatter(formatter)

        # Set cki loglevel to CKI_LOGGING_LEVEL.
        cki_logger = logging.getLogger('cki')
        cki_logger.setLevel(os.environ.get('CKI_LOGGING_LEVEL', 'WARNING'))

    if not (logger_name == 'cki' or logger_name.startswith('cki.')):
        logger_name = 'cki.' + logger_name

    return logging.getLogger(logger_name)
