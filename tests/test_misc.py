"""Tests the misc functions."""

import os
import unittest
from unittest import mock

from dce_common import misc


class TestMisc(unittest.TestCase):
    """Tests the misc functions."""

    def test_deployment_environment(self) -> None:
        """Check deployment_environment functionality."""
        self.assertIs(misc.is_production(), False)
        self.assertEqual(misc.deployment_environment(), 'staging')
        with mock.patch.dict(os.environ, {'DCE_ENVIRONMENT': 'staging'}):
            self.assertIs(misc.is_production(), False)
            self.assertEqual(misc.deployment_environment(), 'staging')
        with mock.patch.dict(os.environ, {'DCE_ENVIRONMENT': 'production'}):
            self.assertIs(misc.is_production(), True)
            self.assertEqual(misc.deployment_environment(), 'production')
        with mock.patch.dict(os.environ, {'DCE_ENVIRONMENT': 'mr-123'}):
            self.assertIs(misc.is_production(), False)
            self.assertEqual(misc.deployment_environment(), 'mr-123')
