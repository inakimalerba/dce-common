"""Tests for HTML output."""

import unittest
from unittest import mock

from dce_common import html


class TestHtml(unittest.TestCase):
    """Tests for HTML output."""

    def test_greeting(self) -> None:
        """Check the greeting."""
        self.assertEqual(html.hello_world('foo'), '<p>Hello world, foo!</p>')

    @mock.patch.dict('os.environ', {'DCE_GREETING': 'hi there'})
    def test_greeting_custom(self) -> None:
        """Check a custom greeting."""
        self.assertEqual(html.hello_world('foo'), '<p>Hi there, foo!</p>')

    def test_farewell(self) -> None:
        """Check the farewell."""
        self.assertEqual(html.goodbye('foo'), '<p>Goodbye, foo!</p>')

    @mock.patch.dict('os.environ', {'DCE_FAREWELL': 'ciao'})
    def test_farewell_custom(self) -> None:
        """Check a custom farewell."""
        self.assertEqual(html.goodbye('foo'), '<p>Ciao, foo!</p>')
